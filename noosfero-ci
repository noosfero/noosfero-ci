#!/bin/sh

set -e

run_cmd() {
  echo "+" "$@"
  timeout 60 "$@"
}

setup() {
  local project="$1"
  local repository="$2"
  local branch="$3"
  local run_dir="$4"
  if [ -d "repositories/$project" ]; then
    echo 'Updating the source code'
    echo '------------------------'
    echo
    cd "repositories/$project"
    local head0="$(git rev-list HEAD --abbrev-commit --max-count=1)"
    echo "$head0" > "$run_dir/head0"
    (
      run_cmd git checkout .
      run_cmd git clean -dxfq
      run_cmd git pull
    )
    local head="$(git rev-list HEAD --abbrev-commit --max-count=1)"
    git rev-list "${head0}..HEAD" | wc -l > "$run_dir/commits"
    echo "$head" > "$run_dir/head"
    (
      run_cmd git log --reverse ${head0}..${head}
      run_cmd git diff --name-status ${head0}..${head}
    )
    echo; echo
  else
    echo 'Cloning source code repository'
    echo '------------------------------'
    (
      run_cmd git clone --branch "$branch" "$repository" "repositories/$project"
    )
    cd "repositories/$project"
    git rev-list HEAD --max-count=1 > "$run_dir/head"
  fi
}

main() {
  local project="$1"
  local repository="$2"
  local branch="$3"
  local build_command="$4"

  local run_id=$(date +%Y/%m/%d/%H%M%S)
  local run_dir="$basedir"/output/$project/$run_id
  mkdir -p "$run_dir"
  log="$run_dir/log.txt"

  if ! setup "$project" "$repository" "$branch" "$run_dir"> "$log" 2>&1; then
    # no exitstatus, no duration!
    return
  fi

  if [ -n "$FORCE" ]; then
    rm -f "$run_dir/head0"
  else
    if [ -f "$run_dir/head0" ]; then
      local head0=$(cat "$run_dir/head0")
      local head=$(cat "$run_dir/head")
      if [ "$head0" = "$head" ]; then
        rm -rf "$run_dir"
        exit 0
      fi
    fi
  fi

  (
    echo 'Running tests'
    echo '-------------'
    echo
  ) >> "$log"

  local rc=0
  local start=$(date +%s)
  export HEAD="$head"
  export PREV_HEAD="$head0"
  local script=$(mktemp)
  echo "$build_command" > "$script"
  chmod +x "$script"
  sh -x $script >> "$log" 2>&1 || rc=$?
  local finish=$(date +%s)
  rm -f "$script"

  echo "$rc" > "$run_dir/exitstatus"
  echo "$(($finish - $start))" > "$run_dir/duration"

  # restore CWD
  cd $basedir

  # tag latest entry
  ln -sfT "$run_id" "$basedir/output/$project/latest"

  if [ "$rc" -eq 0 ]; then
    echo "$HEAD" > "$basedir/output/$project/LAST_SUCCESS_HEAD"
  fi

  ./noosfero-ci-publish "$project"
  ./noosfero-ci-publish

}

export CDPATH=
basedir=$(readlink -f $(dirname $0))
cd "$basedir"

if [ $# -ne 4 ]; then
  echo "usage: PROJECT REPOSITORY BRANCH BUILDCOMMAND"
  exit 1
fi

mkdir -p "$basedir/repositories"

(
  if ! flock -n 9; then
    if [ -t 1 ]; then
      echo "Already running"
    fi
    exit 0
  fi
  main "$@"
) 9> "$basedir/repositories/${1}.lock"
