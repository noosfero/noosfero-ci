# encoding: UTF-8

require 'erb'
require 'fileutils'
require 'pathname'
require 'rss'
require 'yaml'

$TEMPLATES = YAML.load(File.read('templates.yaml'))

class Run
  attr_accessor :log, :date, :status, :duration, :head0, :head, :commits
  attr_accessor :project

  def initialize(logfile, project)
    self.log = logfile
    self.project = project

    dir = File.dirname(logfile)

    self.date = File.stat(logfile).mtime

    exitstatus = File.join(dir, 'exitstatus')
    if File.exists?(exitstatus)
      self.status = case File.read(exitstatus).to_i
                    when 0
                      :pass
                    else
                      :fail
                    end
    else
      self.status = :unknown
      return
    end

    duration_file = File.join(dir, 'duration')
    if File.exists?(duration_file)
      seconds = File.read(duration_file).to_i
      minutes = seconds / 60
      seconds = seconds % 60
      hours = minutes / 60
      minutes = minutes % 60
      self.duration = "%02d:%02d:%02d" % [hours, minutes, seconds]
    end

    self.head0 = read(File.join(dir, 'head0'))
    self.head = read(File.join(dir, 'head'))
    self.commits = read(File.join(dir, 'commits')).to_i
  end

  def headline
    if [:pass, :fail].include?(status)
      "#{project} tests #{status.upcase}ED"
    else
      "#{project} tests: #{status.upcase}"
    end
  end

  private

  def read(file)
    if File.exists?(file)
      File.read(file).strip
    end
  end
end

class CIConfig
  def initialize(data)
    data.each do |key, value|
      if self.respond_to?("#{key}=")
        self.send("#{key}=", value)
      end
    end
  end

  attr_accessor :base_url
  attr_accessor :name
  attr_accessor :repository
  attr_accessor :build_command
  attr_accessor :commit_url
  attr_accessor :compare_url
end

class Repository

  attr_reader :config
  attr_reader :root
  attr_reader :project

  def initialize(root)
    @root = root
    @config = CIConfig.new(load_config)
  end

  def publish_atom
    feed = RSS::Maker.make('atom') do |feed|
      feed.channel.author = 'Noosfero Continuous Integration'
      feed.channel.updated = runs.first.date
      feed.channel.about = config.base_url
      feed.channel.title = project || 'Noosfero Continuous Integration'
      feed.channel.description = "News in Noosfero Continuous integration (status changes only)"

      news = []
      latest_by_project = {}
      runs.reverse.each do |run|
        latest = latest_by_project[run.project]
        if latest && run.status != latest.status
          news.unshift run
        end
        latest_by_project[run.project] = run
      end

      news.each do |run|
        feed.items.new_item do |item|
          log_link = [config.base_url, project, run.log].compact.join('/')
          item.link = log_link
          item.title = run.headline
          item.date = run.date
          item.description = [
            "<strong>Date:</strong> #{run.date}",
            "<strong>Duration:</strong> #{run.duration || '?'}",
            "<strong><a href='#{log_link}'>Log file</a></strong>"
          ].compact.join("<br/>\n")
        end
      end
    end
    File.open(File.join(root, 'atom.xml'), 'w') do |f|
      f.write(feed.to_s.gsub('<summary>', '<summary type="html">'))
    end
  end

  def runs
    @runs ||= []
  end

  def load(how_many)
    Dir.chdir(root) do
      logs = Dir.glob('**/log.txt').sort_by { |f| File.mtime(f) }
      logs.last(how_many).each do |log|
        # most recent on top
        p = project || log.split('/').first
        runs.unshift Run.new(log, p)
      end
    end
  end

  protected

  def load_config
    config_file = ['config.yaml', 'config.yaml.dist'].find do |f|
      File.exists?(f)
    end
    YAML.load_file(config_file)
  end

  def commit_link(sha1)
    href = config.commit_url % sha1
    "<a href='%s'>%s</a>" % [href, sha1[0..7]]
  end

  def compare_link(from, to)
    href = config.compare_url % [from, to]
    "<a href='%s'>→</a>" % href
  end

end

class ProjectRepository < Repository

  attr_reader :root

  def initialize(project, root)
    @project = project
    super(root)
  end

  def publish_html
    contents = ERB.new($TEMPLATES['project_page']).result(binding)
    template = ERB.new($TEMPLATES['layout'])
    File.open(File.join(root, 'index.html'), 'w') do |f|
      f.write(template.result(binding))
    end
  end

  include FileUtils

  def publish_assets
    assets_dir = File.join(File.dirname(__FILE__), 'assets')
    assets = Dir.chdir(assets_dir) do
      Dir.glob('**/*')
    end
    assets.each do |a|
      target = File.join(root, a)
      unless File.exists?(target)
        source = File.join(assets_dir, a)
        mkdir_p(File.dirname(target))
        ln(source, target)
      end
    end
  end

  protected

  def load_config
    data = super
    projects = data.delete('projects')
    project_config = projects.find { |p| p['name'] == project }
    data.merge(project_config)
  end

end

class OverviewRepository < Repository

  def project_statuses
    @project_statuses ||=
      begin
        Dir.chdir(root) do
          Dir.glob('*/latest/log.txt').map do |log|
            project = File.basename(File.dirname(File.dirname(log)))
            Run.new(log, project)
          end
        end
      end
  end

  def publish_home_page
    contents = ERB.new($TEMPLATES['home_page']).result(binding)

    index = root.join('index.html')
    File.open(index, 'w') do |f|
      f.write(ERB.new($TEMPLATES['layout']).result(binding))
    end
  end
end

